public class Dog extends Animals implements Runnable, Swimmable {
    static int dogCount;

    public Dog(String name) {
        super(name);
        dogCount++;
    }


    public void run(int trackLength) {
        if (trackLength <= 500) {
            System.out.println(name + " probig " + trackLength + " m");
        } else {
            distanceTooLong();
        }
    }

    @Override
    public void swim(int trackLength) {
        if (trackLength <= 10) {
            System.out.println(name + " proplyv " + trackLength + " m");
        } else {
            distanceTooLong();
        }
    }
}
