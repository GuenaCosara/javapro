public interface Swimmable {
    public void swim(int trackLength);
}
