public class Cat extends Animals implements Runnable, Swimmable {

    static int catCount;

    public Cat(String name) {
        super(name);
        catCount++;
    }

    @Override
    public void run(int trackLength) {
        if (trackLength <= 200) {
            System.out.println(name + " probig " + trackLength + " m");
        } else {
            distanceTooLong();
        }
    }

    @Override
    public void swim(int trackLength) {
        System.out.println("Koty ne plavaut");
    }
}
