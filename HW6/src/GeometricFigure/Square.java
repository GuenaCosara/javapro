package GeometricFigure;

public class Square implements Areable {
    double side;
    double height;
    public Square(double side) {
        this.side = side;
    }

    @Override
    public double calculateArea() {
        return side * 4;
    }
}
