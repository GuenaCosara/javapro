package GeometricFigure;

public class Triangle implements Areable {

    double longestSide;
    double height;

    public Triangle(double longestSide, double height) {
        this.longestSide = longestSide;
        this.height = height;
    }

    @Override
    public double calculateArea() {
        return longestSide * height / 2;
    }
}
