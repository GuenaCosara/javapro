package GeometricFigure;

public class Main {

    static Areable[] figures = {
            new Circle(2),
            new Triangle(2, 3),
            new Square(2)
    };

    public static void main(String[] args) {
        calculateTotalArea();
    }

    public static double calculateTotalArea() {
        double totalArea = 0;
        for (Areable element : figures) {
            totalArea += element.calculateArea();
        }
        return totalArea;
    }
}
