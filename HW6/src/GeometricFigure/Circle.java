package GeometricFigure;

public class Circle implements Areable {
    double radius;
    public Circle(double radius) {
        this.radius = radius;
    }

    @Override
    public double calculateArea() {
        return Math.PI * radius;
    }
}
