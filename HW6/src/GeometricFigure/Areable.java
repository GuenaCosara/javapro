package GeometricFigure;

public interface Areable {
    double calculateArea();
}