package Competition;

public class Participant {
    String name;
    int runCapacity;
    int jumpCapacity;

    public Participant(String name, int runCapacity, int jumpCapacity) {
        this.name = name;
        this.runCapacity = runCapacity;
        this.jumpCapacity = jumpCapacity;
    }

    public void action(String obstacleType, int obstacleDistance) {
        System.out.println(
                "Participant " + name + " has passed the obstacle " + obstacleType + " at the distance " + obstacleDistance + "."
        );
    }

    public void actionFailed(String obstacleType, int obstacleDistance) {
        System.out.println(
                "Participant " + name + " did not pass the obstacle " + obstacleType + " at the distance " + obstacleDistance + ". Passed " + runCapacity
        );
    }

    public void jump(String obstacleType, int obstacleDistance) {
        System.out.println(
                "Participant " + name + " jumped over the " + obstacleType + " at the distance " + obstacleDistance + "."
        );
    }

    public void jumpFailed(String obstacleType, int obstacleDistance) {
        System.out.println(
                "Participant " + name + " did not pass the obstacle " + obstacleType + " at the distance " + obstacleDistance + ". Passed " + runCapacity
        );
    }
}
