package Competition;

import java.lang.reflect.Array;

public class Competition {
    static Participant[] participants = {
            new Participant("robot", 9, 10),
            new Participant("human", 12, 13),
            new Participant("cat", 6, 5)
    };

    static Obstacle[] obstacles = {
            new Obstacle("racetrack", 11),
            new Obstacle("wall", 12)
    };

    public static void main(String[] args) {
        for (Participant participant : participants) {
            for (Obstacle obstacle : obstacles) {

                if (obstacle.type == "racetrack") {
                    if (obstacle.obstacleDistance > participant.runCapacity) {
                        participant.actionFailed(obstacle.type, obstacle.obstacleDistance);
                        break;
                    } else {
                        participant.action(obstacle.type, obstacle.obstacleDistance);
                    }
                } else {
                    if (obstacle.obstacleDistance > participant.jumpCapacity) {
                        participant.jumpFailed(obstacle.type, obstacle.obstacleDistance);
                        break;
                    } else {
                        participant.jump(obstacle.type, obstacle.obstacleDistance);
                    }
                }
            }
        }
    }
}
