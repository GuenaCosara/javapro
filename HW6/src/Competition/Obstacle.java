package Competition;

public class Obstacle {
    String type;
    int obstacleDistance;

    public Obstacle(String type, int obstacleDistance) {
        this.type = type;
        this.obstacleDistance = obstacleDistance;
    }
}
