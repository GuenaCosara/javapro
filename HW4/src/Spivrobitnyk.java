public class Spivrobitnyk {
    private String pib;
    private String posada;
    private String email;
    private String telefon;
    private short vik;

    private Spivrobitnyk(String pib, String posada, String email, String telefon, short vik) {
        this.setPib(pib);
        this.setPosada(posada);
        this.setEmail(email);
        this.setTelefon(telefon);
        this.setVik(vik);
    }

    public String getPib() {
        return pib;
    }

    public void setPib(String pib) {
        this.pib = pib;
    }

    public String getPosada() {
        return posada;
    }

    public void setPosada(String posada) {
        this.posada = posada;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTelefon() {
        return telefon;
    }

    public void setTelefon(String telefon) {
        this.telefon = telefon;
    }

    public short getVik() {
        return vik;
    }

    public void setVik(short vik) {
        this.vik = vik;
    }
}
